<?php


use App\Models\Imoveis;
use App\Http\Controllers\ImoveisController;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
   Illuminate\Support\Facades\Log::info($query->sql); // Dumps sql
   Illuminate\Support\Facades\Log::info("parametros",$query->bindings); //Dumps data passed to query
   Illuminate\Support\Facades\Log::info("Tempo de execução da query ".$query->time." ms"); //Dumps time sql took to be processed
});



$router->get('/publicdoc', function () use ($router) {
    return include ('doc/index.html');
});

$router->get('api/sugestao', 'ImoveisController@getSugestao');
$router->get('api/buscaAvancada', 'ImoveisController@getBuscaAvancada');
$router->get('api/features', 'ImoveisController@getFeatures');
$router->get('api/bairros', 'ImoveisController@getBairros');

/**
 * @api {get} api/bairros Listar Bairros
 * @apiName listaBairros
 * @apiDescription Tras a lista de todos os bairros.
 * @apiGroup Bairros
 * @apiSuccess (Bairro) {integer} id  Id do bairro.
 * @apiSuccess (Bairro) {String} nome  Nome do Bairro.
 * @apiSuccess (Bairro) {integer} cidade_id  Id da cidade a qual o bairro pertence.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     [
    {
        "id": 19,
        "nome": "Jardim Eliana A",
        "cidade_id": 4850,
    },
    {
        "id": 27,
        "nome": "Aclimação",
        "cidade_id": 4850,
    },
    {
        "id": 35,
        "nome": "Água Branca",
        "cidade_id": 4850,
    }
        ]
    }
 */

    $router->get('api/imoveis/tipos', 'ImoveisController@getImoveisTipos');

/**
 * @api {get} api/imoveis/tipo/{tipo} Imoveis por tipo
 * @apiName imoveisBusca
 * @apiDescription Lista imoveis de acordo com tipo descrito.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
     "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

        $router->get('api/imoveis', 'ImoveisController@getImoveisTiposID');

/**
 * @api {get} api/imoveis/tipos Numero de imoveis divididos por tipo.
 * @apiName tiposImoveis
 * @apiDescription mostra todos os tipos de imoveis e suas quantidades.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do tipo de imovel.
 * @apiSuccess (Parameter) {String} nome  Nome do tipo.
 * @apiSuccess (Parameter) {String} Qtd  Quantidade de imoveis do tipo descrito em nome.
 
 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     [
    {
        {
            "id": 3,
            "nome": "LOJA"
        },
        "Qtd": 64
    },
    {
        {
            "id": 4,
            "nome": "TERRENO"
        },
        "Qtd": 680
    },
    {
        {
            "id": 9,
            "nome": "GALPAO"
        },
        "Qtd": 271
    },
    {
        {
            "id": 11,
            "nome": "COMERCIAL"
        },
        "Qtd": 913
    },
    {
        {
            "id": 12,
            "nome": "OUTROS"
        },
        "Qtd": 42
    },
    {
        {
            "id": 13,
            "nome": "CONJUNTO COMERCIAL"
        },
        "Qtd": 239
    },
    {
            "id": 36,
            "nome": "APARTAMENTO"
        },
        "Qtd": 4125
    },
    {
            "id": 40,
            "nome": "SOBRADO"
        },
        "Qtd": 3358
    }
    ]
    */


    $router->get('/api/busca', 'ImoveisController@getImoveisBuscaBC');

/**
 * @api {get} /api/busca/{chave}/{tipo}/{fin} Busca de imoveis detalhada
 * @apiName imoveisBuscaDetalhada
 * @apiDescription busca unificada em cidades e bairros.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Parameter) {String} categoria_tipo_imovel  Categoria de imovel. ex.: Casa, Apartamento, Sobrado.
 * @apiSuccess (Parameter) {String} finalidade_id  Tipo de negociação a qual o imovel esta sendo ofertado. 1-Venda, 2-Locação e 3-Venda e Locação.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
     "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "categoria_tipo_imovel": 40,
            "finalidade_id": 1,
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

        $router->get('api/cidades', 'ImoveisController@getCidades');

/**
 * @api {get} api/cidades Listar Cidades
 * @apiName listaCidades
 * @apiDescription Tras a lista de todos as cidades.
 * @apiGroup Cidades
 * @apiSuccess (Cidade) {integer} id  Id da cidade.
 * @apiSuccess (Cidade) {integer} codigo_municipio  Codigo de Municipio.
 * @apiSuccess (Cidade) {String} nome  Nome da Cidade.
 * @apiSuccess (Cidade) {String} uf  UF da Cidade.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     [
    {
        "id": 1,
        "estado_id": 9,
        "codigo_municipio": 5200050,
        "nome": "Abadia de Goiás",
        "uf": "GO"
    },
    {
        "id": 2,
        "estado_id": 13,
        "codigo_municipio": 3100104,
        "nome": "Abadia dos Dourados",
        "uf": "MG"
    },
    {
        "id": 3,
        "estado_id": 9,
        "codigo_municipio": 5200100,
        "nome": "Abadiânia",
        "uf": "GO"
    },
    {
        "id": 4,
        "estado_id": 13,
        "codigo_municipio": 3100203,
        "nome": "Abaeté",
        "uf": "MG"
    }
        ]
    }
 */

    $router->get('api/destaques', 'ImoveisController@getDestaques');
    $router->get('api/destaques', 'ImoveisController@getDestaques');

/**
 * @api {get} api/destaques Lista Imoveis para destaque
 * @apiName listaImoveisDestaque
 * @apiDescription Tras a lista de todos os imoveis marcados como destaque.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
 *      {
    "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

        $router->get('/api', 'ImoveisController@index');

/**
 * @api {get} /api Lista Imoveis 
 * @apiName listaImoveis
 * @apiDescription Tras a lista de todos os imoveis.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
     "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

        $router->get('/api/imoveis/{id}', 'ImoveisController@getImoveis');
/**
 * @api {get} /api/imoveis/{id} Busca de Imoveis
 * @apiName imoveisPorID
 * @apiDescription Busca Imovel pelo ID
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
     "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

        $router->get('/api/imoveisvenda', 'ImoveisController@getImoveisVenda');

/**
 * @api {get} api/imoveisvenda Lista Imoveis para venda
 * @apiName listaImoveisVenda
 * @apiDescription Tras a lista de todos os imoveis com valor de venda.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
     "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

        $router->get('/api/imoveislocacao', 'ImoveisController@getImoveisLocacao');

/**
 * @api {get} api/imoveislocacao Lista Imoveis para locação
 * @apiName listaImoveisLocacao
 * @apiDescription Tras a lista de todos os imoveis com valor de locação.
 * @apiGroup Imoveis
 * @apiSuccess (Parameter) {integer} id  Id do imovel e também Codigo do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cep  CEP do Imovel .
 * @apiSuccess (Parameter) {String} endereco_logradouro  Logradouro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_numero  Numero do Imovel.
 * @apiSuccess (Parameter) {String} endereco_complemento  Complemento do Imovel.
 * @apiSuccess (Parameter) {String} endereco_bairro_id ID do bairro do Imovel.
 * @apiSuccess (Parameter) {String} endereco_cidade_id Referente ao ID da cidade do imovel.
 * @apiSuccess (Parameter) {tinyInt} dormitorios  Quantidades de dormitorios que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} suites   Quantidades de suites que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} wc  Quantidades de banheiros que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} salas   Quantidades de salas que o Imovel possui.
 * @apiSuccess (Parameter) {tinyInt} vagas   Quantidades de vagas de garagem que o Imovel possui.
 * @apiSuccess (Parameter) {float} area_util Area Util do Imovel.
 * @apiSuccess (Parameter) {float} area_comun Area Comun do Imovel.
 * @apiSuccess (Parameter) {float} area_construida Area Construida do Imovel.
 * @apiSuccess (Parameter) {float} area_total Area Total do Imovel.
 * @apiSuccess (Parameter) {float} area_terreno Area do terreno.
 * @apiSuccess (Parameter) {String} metragem_terreno  Numero do Imovel.
 * @apiSuccess (Parameter) {decimal} valor_venda  Valor de venda do imovel.
 * @apiSuccess (Parameter) {decimal} valor_locacao  Valor de Aluguel do imovel.
 * @apiSuccess (Parameter) {decimal} valor_condominio  Valor do condominio do imovel.
 * @apiSuccess (Parameter) {String} valor_iptu  Valor do IPTU do imovel.
 * @apiSuccess (Parameter) {text} descricao_completa  Descricao completa do imovel.
 * @apiSuccess (Parameter) {integer} agencia_id  Id referente a agencia onde esse imovel foi publicado.
 * @apiSuccess (Parameter) {String} created_at  Registro referente a quando o imovel foi criado.
 * @apiSuccess (Parameter) {String} updated_at  Registro referente a quando o imovel foi editado.
 * @apiSuccess (Photos) {integer} imovel_fotos  Id da foto.
 * @apiSuccess (Photos) {String} arquivo  Nome do arquivo da imagem do imovel que deve ser complementado com "https://www.mirantte.com.br/FotosImovel".
 * @apiSuccess (Photos) {String} created_at  Registro referente a quando o arquivo foi criado.
 * @apiSuccess (Photos) {String} updated_at  Registro referente a quando o arquivo foi editado.
 * @apiSuccess (Photos) {integer} imovel_id  Numero referente ao id do imovel pertencente a foto.



 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
     "current_page": 1,
    "data": [
        {
            "id": 150821,
            "endereco_cep": "02451-040",
            "endereco_logradouro": "Rua Francisca Biriba",
            "endereco_numero": "431",
            "endereco_complemento": " ",
            "endereco_bairro_id": 11037,
            "endereco_cidade_id": 4850,
            "dormitorios": 2,
            "suites": 2,
            "wc": 3,
            "salas": 2,
            "vagas": 5,
            "area_util": 249,
            "area_comum": null,
            "area_construida": 249,
            "area_total": null,
            "area_terreno": 150,
            "metragem_terreno": "5x30",
            "valor_venda": "850000.00",
            "valor_locacao": "0.00",
            "valor_condominio": "0.00",
            "valor_iptu": "2560.00",
            "descricao_completa": "Oito anos de construção com telhado de cimento,aquecimento solar,tubulação de gás e tubulação pronta para ar condicionado,lavabo,lustres e acabamento com gesso, cerca elétrica,portão automático,piscina...entrar e morar!!!!",
            "agencia_id": 5,
            "created_at": "2015-03-02 00:00:00",
            "updated_at": "2018-03-26 16:33:07",
            "imovel_fotos": [
                {
                    "id": 278181,
                    "arquivo": "001_00000_00000_150821_2015030518093749.jpg",
                    "indice": 9,
                    "descricao": null,
                    "created_at": "2015-10-15 03:15:00",
                    "updated_at": "2015-10-15 03:15:00",
                    "imovel_id": 150821
                }

            ]
        }
    */

