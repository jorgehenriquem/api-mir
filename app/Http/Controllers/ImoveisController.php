<?php

namespace App\Http\Controllers;

use App\Models\Imoveis;
use App\Models\Cidades;
use App\Models\Bairros;
use App\Models\imovel_fotos;
use App\Models\Atributos;
use App\Models\categorias_tipo_imovel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Traits\ImoveisTrait;


class ImoveisController extends Controller
{

    use Imoveistrait;

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
    }

    public function getSugestao()
    {
        $sugestao = Imoveis::getSugestao();
        if (count($sugestao) == 0) {
            return response('204', 204)->header('Content-Type', 'text/plain');
        }
        return response()->json($sugestao);
    }

    public function getImoveis($id)
    {
        $idImovel = imoveis::find($id);
        $findImoveis = imoveis::with('Atributos:nome')->select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'finalidade_id', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at', 'endereco_map_lat', 'endereco_map_long', 'youtubeurl', 'tourvirtual', 'valor_promo')->with('imovel_fotos')->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)))->find($id);

        if (count($findImoveis) == 0) {
            return response('204', 204)->header('Content-Type', 'text/plain');
        }
        return response()->json($findImoveis);
    }


    public function getCidades()
    {

        $lista = cidades::all();

        return response()->json($lista);
    }

    public function getFeatures()
    {

        $features = Atributos::select('id', 'nome')->whereIn('id', [
            1, 8, 29, 16, 77, 80, 23, 14, 28, 15, 79, 76
        ])->orderBy('nome')->get();

        return response()->json($features);
    }


    public function getBairros()
    {
        $lista = bairros::all();
        return response()->json($lista);
    }

    public function getDestaques()
    {
        $Imoveis = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero',
            'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas',
            'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno',
            'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id',
            'finalidade_id', 'created_at', 'updated_at', 'tourvirtual', 'valor_promo'
        )
            ->with('imovel_fotos')
            ->where('destaque', '=', 1)
            ->whereIn('finalidade_id', [
                1, 3
            ]
            )
            ->where('imovel_status_id', '=', 4)
            ->whereIn('situacao_id', [
                51, 52
            ]
            )
            ->whereNotNull('sitesync')
            ->paginate(30);
        return response()->json($Imoveis);
    }


    public function getImoveisVenda(Request $request)
    {

        $query = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at')
            ->with('imovel_fotos')->whereIn('finalidade_id', [1, 3])->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->where('valor_venda', '!=', 0)->whereNotNull('valor_venda')->whereNotNull('sitesync')->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));

        if ($request->has('sortby')) {
            if ($request->input('sortby') == 'a_preco')
                $query->orderBy('valor_venda', 'DESC');
            if ($request->input('sortby') == 'b_preco')
                $query->orderBy('valor_venda', 'asc');
            if ($request->input('sortby') == 'a_date')
                $query->orderBy('updated_at', 'asc');
            if ($request->input('sortby') == 'n_date')
                $query->orderBy('updated_at', 'desc');
        } else {
            $query->orderBy('updated_at', 'desc');
        }
        return response()->json($query->paginate(15)->appends($request->input()));
    }


    public function getImoveisLocacao(Request $request)
    {

        $query = imoveis::select(
            'id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id',
            'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum',
            'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao',
            'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at')
            ->with('imovel_fotos')
            ->whereIn('finalidade_id', [3, 2])
            ->where('imovel_status_id', '=', 4)
            ->whereIn('situacao_id', [51, 52])
            ->where('valor_locacao', '!=', 0)
            ->whereNotNull('valor_locacao')
            ->whereNotNull('sitesync')
            ->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));
        if ($request->has('sortby')) {
            if ($request->input('sortby') == 'a_preco')
                $query->orderBy('valor_locacao', 'DESC');
            if ($request->input('sortby') == 'b_preco')
                $query->orderBy('valor_locacao', 'asc');
            if ($request->input('sortby') == 'a_date')
                $query->orderBy('updated_at', 'asc');
            if ($request->input('sortby') == 'n_date')
                $query->orderBy('updated_at', 'desc');
        } else
            $query->orderBy('updated_at', 'desc');

        return response()->json($query->paginate(15)->appends($request->input()));
    }

    public function index()
    {
        //$idImovel=imoveis::all('id');
        $findImoveis = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at')->with('imovel_fotos')->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->paginate(15);
        return response()->json($findImoveis);

    }

    public function getImoveisCidade($cidade)
    {

        $findImoveis = cidades::where('nome', '=', urldecode($cidade))->first();
        $ImovelConsulta = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at')->with('imovel_fotos')->where('endereco_cidade_id', '=', $findImoveis->id)->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->paginate(15);

        return response()->json($ImovelConsulta);
    }

    public function getImoveisBairro($bairro)
    {

        $findImoveis = bairros::where('nome', '=', urldecode($bairro))->first();
        $ImovelConsulta = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at')->with('imovel_fotos')->where('endereco_bairro_id', '=', $findImoveis->id)->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->paginate(15);

        return response()->json($ImovelConsulta);
    }

    public function getImoveisBusca($chave)
    {


        $findCidades = cidades::where('nome', 'LIKE', '%' . urldecode($chave) . '%')
            ->get(['id']);
        $findBairros = bairros::where('nome', 'LIKE', '%' . urldecode($chave) . '%')
            ->get(['id']);


        if ($findCidades != "[]" and $findBairros != "[]") {
            foreach ($findCidades as $fC) {
                $IC[] = $fC->id;
            }
            foreach ($findBairros as $fB) {
                $IB[] = $fB->id;
            }


            $ImovelConsultaC = imoveis::select(
                'id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento',
                'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas',
                'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno',
                'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id',
                'created_at', 'updated_at', 'categoria_tipo_imovel', 'finalidade_id'
            )
                ->WhereIn('endereco_bairro_id', $IB)
                ->with('imovel_fotos')
                ->where('imovel_status_id', '=', 4)
                ->whereIn('situacao_id', [51, 52])
                ->whereNotNull('sitesync')
                ->orWhereIn('endereco_cidade_id', $IC)
                ->with('imovel_fotos')
                ->where('imovel_status_id', '=', 4)
                ->whereIn('situacao_id', [51, 52])
                ->whereNotNull('sitesync')
                ->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));
            $ij = ($ImovelConsultaC)->paginate(10);


        } elseif (isset($findCidades) and empty($findBairros)) {


            foreach ($findCidades as $fC) {

                $IC[] = $fC->id;
            }


            $ImovelConsultaC = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at', 'categoria_tipo_imovel', 'finalidade_id')->with('imovel_fotos')
                ->whereIn('endereco_cidade_id', $IC)->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));


            $ij = ($ImovelConsultaC)->paginate(10);


        } elseif (empty($findCidades) and isset($findBairros)) {


            foreach ($findBairros as $fB) {

                $IB[] = $fB->id;
            }
            imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at', 'categoria_tipo_imovel', 'finalidade_id')->with('imovel_fotos')
                ->whereIn('endereco_bairro_id', $IB)->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));


            $ij = ($ImovelConsultaC)->paginate(10);


        } else {

            $ij = '204';

            return response('204', 204)->header('Content-Type', 'text/plain');

        }

        return response()->json($ij);
    }

    public function getImoveisBuscaBC(Request $request)
    {
        $imoveis = (new Imoveis)->getDadosImoveis($request);
        if (count($imoveis) == 0) {
            return response('204', 204)->header('Content-Type', 'text/plain');
        }
        return response()->json($imoveis);
    }

    public function getImoveisTiposID(Request $request)
    {

        $tipo = $request->input('tipo');
        $findImoveis = categorias_tipo_imovel::where('id', '=', urldecode($tipo))->first();
        $query = imoveis::select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'finalidade_id', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at')->with('imovel_fotos')->where('categoria_tipo_imovel', '=', $findImoveis->id)->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));

        if ($request->has('sortby')) {

            if ($request->input('sortby') == 'b_preco') {
                $query->orderBy('valor_venda', 'asc');
                $query->orderBy('valor_locacao', 'asc');
            } elseif ($request->input('sortby') == 'a_preco') {
                $query->orderBy('valor_venda', 'desc');
                $query->orderBy('valor_locacao', 'desc');
            } elseif ($request->input('sortby') == 'a_date')
                $query->orderBy('updated_at', 'asc');

            elseif ($request->input('sortby') == 'n_date')
                $query->orderBy('updated_at', 'desc');
        } else
            $query->orderBy('updated_at', 'desc');

        return response()->json($query->paginate(15)->appends($request->input()));

    }

    public function getImoveisTipos()
    {

        $I = categorias_tipo_imovel::select(DB::raw('count(*) as total, categorias_tipo_imovel.id,nome'))->join('imoveis', 'categorias_tipo_imovel.id', '=', 'imoveis.categoria_tipo_imovel')->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->groupby('categorias_tipo_imovel.id', 'nome')->orderBy('nome')->get();


        return response()->json($I);

    }

    public function getCountTipo($countTipo)
    {

        $findImoveis = categorias_tipo_imovel::where('id', '=', urldecode($countTipo))->first();
        $ImovelConsulta = imoveis::where('categoria_tipo_imovel', '=', $findImoveis->id)->where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->count();

        return response()->json($ImovelConsulta);

    }

    public function getCountAll()
    {

        $ImovelConsulta = imoveis::where('imovel_status_id', '=', 4)->whereIn('situacao_id', [51, 52])->whereNotNull('sitesync')->count();

        return response()->json($ImovelConsulta);

    }

    public function getBuscaAvancada(Request $request)
    {
        $result = (new Imoveis)->getDadosPesquisaAvancada($request);
        if (count($result) == 0) {
            return response('204', 204)->header('Content-Type', 'text/plain');
        }
        return response()->json($result, 200);
    }


}