<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cidades;
use App\Models\Imoveis;

class Atributos extends Model
{
    //protected $guarded = ['id'];

    public function Imoveis()
    {
        return $this->belongsToMany('App\Models\Imoveis', 'imovel_atributos', 'atributo_id', 'imovel_id')->withPivot('nome');
    }
}