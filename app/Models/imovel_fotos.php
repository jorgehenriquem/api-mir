<?php

namespace App\Models;

use App\Models\Imoveis;
use Illuminate\Database\Eloquent\Model;

Class imovel_fotos extends Model
{
	public function Imoveis()
	{
		return $this->belongsTo('App\Models\Imoveis', 'id', 'imovel_id');
	}
}
