<?php

namespace App\Models;

use App\Models\Bairros;
use App\Models\Imoveis;
use Illuminate\Database\Eloquent\Model;

class Cidades extends Model
{
    //protected $guarded = ['id'];
	public function bairros()
	{
		return $this->hasMany('App\Models\Bairros', 'cidade_id', 'id');
	}

	public function imoveis()
	{
		return $this->hasMany('App\Models\Imoveis', 'endereco_cidade_id', 'id');
	}
}