<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cidades;
use App\Models\categorias_tipo_imovel;

class tipos_imovel extends Model
{
    //protected $guarded = ['id'];

    protected $table = "tipos_imovel";

	public function categorias_tipo_imovel()
	{
		return $this->belongsTo('App\Models\categorias_tipo_imovel', 'categoria_tipo_imovel_id', 'id');
	}
	

}