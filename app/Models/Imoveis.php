<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\imovel_fotos;
use App\Models\Cidades;
use App\Models\Bairros;
use App\Models\Atributos;
use App\Models\tipos_imovel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Imoveis extends Model
{
    //protected $guarded = ['id'];
    public function imovel_fotos()
    {
        return $this->hasMany('App\Models\imovel_fotos', 'imovel_id', 'id');
    }

    public function Atributos()
    {
        return $this->belongsToMany('App\Models\Atributos', 'imovel_atributos', 'imovel_id', 'atributo_id');
    }

    public function cidades()
    {
        return $this->belongsTo('App\Models\Cidades', 'endereco_cidade_id', 'id');
    }

    public function bairros()
    {
        return $this->belongsTo('App\Models\Bairros', 'endereco_bairro_id', 'id');
    }

    public static function getSugestao()
    {
        return Bairros::select('bairros.id', 'nome as text')
            ->whereHas('cidades', function ($query) {
                $query->where('estado_id', 25);
            }
            )
            ->join('imoveis', 'bairros.id', '=', 'imoveis.endereco_bairro_id')
            ->where('imovel_status_id', '=', 4)
            ->whereIn('situacao_id', [51, 52])
            ->whereNotNull('sitesync')
            ->distinct()->orderBy('nome')->get();
    }

    public function getDadosImoveis($request)
    {
        $query = Imoveis::with('imovel_fotos')->select('id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento', 'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas', 'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno', 'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id', 'created_at', 'updated_at', 'categoria_tipo_imovel', 'finalidade_id')
            ->where('imovel_status_id', '=', 4)->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)));
        if ($request->has('tipo_imovel_id')) {
            $tipos = tipos_imovel::select('id')->where('categoria_tipo_imovel_id', $request->tipo_imovel_id)->get();
            $query->whereIn('tipo_imovel_id', $tipos);
            //$query->where('categoria_tipo_imovel', '=', $request->tipo_imovel_id);

        }
        $query->whereIn('finalidade_id', [$request->finalidade_id, 3]);
        $query->whereIn('situacao_id', [51, 52]);
        $query->whereNotNull('sitesync');
        if ($request->has('keyword')) {
            $query->where(function ($query) use ($request) {
                $query->whereHas('bairros', function ($query) use ($request) {
                    $query->where('nome', 'LIKE', '%' . urldecode($request->keyword) . '%');
                })
                    ->orWhereHas('cidades', function ($query) use ($request) {
                        $query->where('nome', 'LIKE', '%' . urldecode($request->keyword) . '%');
                    });
            });
        }

        if ($request->finalidade_id == 1) {
            $query->where('valor_venda', '!=', 0);
            $query->whereNotNull('valor_venda');
        }
        if ($request->finalidade_id == 2) {
            $query->where('valor_locacao', '!=', 0);
            $query->whereNotNull('valor_locacao');
        }

        if ($request->has('sortby')) {

            if ($request->input('sortby') == 'b_preco' and $request->finalidade_id == 1)
                $query->orderBy('valor_venda', 'asc');

            elseif ($request->input('sortby') == 'a_preco' and $request->finalidade_id == 1)
                $query->orderBy('valor_venda', 'desc');

            elseif ($request->input('sortby') == 'b_preco' and $request->finalidade_id == 2)
                $query->orderBy('valor_locacao', 'asc');

            elseif ($request->input('sortby') == 'a_preco' and $request->finalidade_id == 2)
                $query->orderBy('valor_locacao', 'desc');

            elseif ($request->input('sortby') == 'a_date')
                $query->orderBy('updated_at', 'asc');

            elseif ($request->input('sortby') == 'n_date')
                $query->orderBy('updated_at', 'desc');
        } else
            $query->orderBy('updated_at', 'desc');

        return $query->paginate(10);

    }

    public function getDadosPesquisaAvancada($request)
    {
        $query = Imoveis::with('imovel_fotos:imovel_id,arquivo', 'Atributos:nome')
            ->select('imoveis.id', 'endereco_cep', 'endereco_logradouro', 'endereco_numero', 'endereco_complemento',
                'endereco_bairro_id', 'endereco_cidade_id', 'dormitorios', 'suites', 'wc', 'salas', 'vagas',
                'area_util', 'area_comum', 'area_construida', 'area_total', 'area_terreno', 'metragem_terreno',
                'valor_venda', 'valor_locacao', 'valor_condominio', 'valor_iptu', 'descricao_completa', 'agencia_id',
                'imoveis.created_at', 'imoveis.updated_at', 'categoria_tipo_imovel', 'finalidade_id')
            ->where('imovel_status_id', '=', 4)
            ->where('updated_at', '>=', (\Carbon\Carbon::now()->subDays(121)))
            ->whereIn('situacao_id', [51, 52])
            ->whereNotNull('sitesync');

        if ($request->has('keyword')) {
            $keyword = $request->input('keyword');
            $query->whereIn('endereco_bairro_id', $keyword);
        }

        if ($request->has('tipo_imovel_id')) {
            $tipos = tipos_imovel::select('id')->where('categoria_tipo_imovel_id', $request->tipo_imovel_id)->get();
            $query->whereIn('tipo_imovel_id', $tipos);
            //$query->where('categoria_tipo_imovel',  $request->input('tipo_imovel_id'));
        }


        if ($request->has('finalidade_id') and $request->input('finalidade_id') != 3) {
            $finalidade = $request->input('finalidade_id');
            $query->whereIn('finalidade_id', [$request->input('finalidade_id'), 3]);
        } else {
            $finalidade = 3;

        }

        $fieldValue = 'valor_locacao';
        if ($finalidade == 2) {
            $query->where('valor_locacao', '!=', 0);
            $query->whereNotNull('valor_locacao');
        }

        if ($finalidade == 1) {
            $fieldValue = 'valor_venda';
            $query->where('valor_venda', '!=', 0);
            $query->whereNotNull('valor_venda');
        }


        if ($request->has('precomin')) {
            $valorMin = $request->input('precomin');
        }

        if ($request->has('precomax')) {
            $valorMax = $request->input('precomax');
        }

        if ($finalidade == 3) {

            $query->whereIn('finalidade_id', [1, 2, 3]);
            //caso coloque validacao do valor de locacao e venda diferente de zero ou nulo, alguns imoveis de finalidade 1  ou 2 nao serão retornados.
            /*  $query->where('valor_locacao','!=',0);
                $query->whereNotNull('valor_locacao');
                $query->where('valor_venda', '!=', 0);
                $query->whereNotNull('valor_venda'); */

            if (!empty($valorMin)) {
                $query->where('valor_venda', '>=', $valorMin);
                $query->Where('valor_locacao', '>=', $valorMin);
            }

            if (!empty($valorMax)) {
                $query->where('valor_venda', '<=', $valorMax ?: '(SELECT MAX(valor_venda) FROM  imoveis)');
                $query->where('valor_locacao', '<=', $valorMax ?: '(SELECT MAX(valor_locacao) FROM  imoveis)');
            }
        }


        if ($finalidade != 3) {
            if (!empty($valorMin)) {
                $query->where($fieldValue, '>=', $valorMin);
            }

            if (!empty($valorMax)) {
                $query->where($fieldValue, '<=', $valorMax);
            }
        }

        if ($request->has('vagas')) {
            $query->where('vagas', $request->input('vagas'));
        }

        if ($request->has('suites')) {
            $query->where('suites', $request->input('suites'));
        }

        if ($request->has('dormitorios')) {
            $query->where('dormitorios', $request->input('dormitorios'));
        }

        if ($request->has('wc')) {
            $query->where('wc', $request->input('wc'));
        }

        if ($request->has('areamin')) {
            $query->where('area_total', '>=', $request->input('areamin'));
        }

        if ($request->has('areamax')) {
            $query->where('area_total', '<=', $request->input('areamax'));
        }

        if ($request->input('features')) {
            $features = $request->input('features');
            $query->whereHas('Atributos', function ($query) use ($features) {
                if (is_array($features)) {
                    $query->whereIn('id', $features);
                }
            });
        }

        if ($request->has('360_video')) {
            $query->whereNotNull('tourvirtual');
        }

        if ($request->has('drone')) {
            $query->whereNotNull('youtubeurl');
        }

        if ($request->has('sortby') and $finalidade != 3) {

            if ($request->input('sortby') == 'b_preco')
                $query->orderBy($fieldValue, 'asc');

            elseif ($request->input('sortby') == 'a_preco')
                $query->orderBy($fieldValue, 'desc');

            elseif ($request->input('sortby') == 'a_date')
                $query->orderBy('updated_at', 'asc');

            elseif ($request->input('sortby') == 'n_date')
                $query->orderBy('updated_at', 'desc');
        } elseif ($request->has('sortby') and $finalidade == 3) {

            if ($request->input('sortby') == 'b_preco') {
                $query->orderBy('valor_venda', 'asc');
                $query->orderBy('valor_locacao', 'asc');
            } elseif ($request->input('sortby') == 'a_preco') {
                $query->orderBy('valor_venda', 'desc');
                $query->orderBy('valor_locacao', 'desc');
            }
        } else
            $query->orderBy('updated_at', 'desc');

        return $query->paginate(10)->appends($request->input());
    }


}


