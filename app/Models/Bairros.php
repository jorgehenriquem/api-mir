<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cidades;
use App\Models\Imoveis;

class Bairros extends Model
{
    //protected $guarded = ['id'];
	public function cidades()
	{
		return $this->belongsTo('App\Models\Cidades', 'cidade_id', 'id');
	}
	public function Imoveis()
	{
		return $this->HasMany('App\Models\Imoveis', 'endereco_bairro_id', 'id');
	}

}