<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tipos_imovel;


class categorias_tipo_imovel extends Model


{
    //protected $guarded = ['id'];
	
    protected $table = "categorias_tipo_imovel";
    
    public function tipos_imovel()
	{
		return $this->hasMany('App\Models\tipos_imovel', 'categoria_tipo_imovel_id', 'id');
	}
    
}


